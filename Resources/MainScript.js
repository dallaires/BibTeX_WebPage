var orderBy = ["Category", "Publisher", "UrlDate", "Year"];
var orderBySecond = ["BibTexRef"];

InitArticleContainer(ArticleList);

function Display(){
    var index = $('#FirstOrder').val();
    var index2 = $('#SecondOrder').val();
    var asc = $('#Asc').is(":checked");
    var asc2 = $('#Asc2').is(":checked");

    var ArticleListSorted = InitArticlesByMappedKey(ArticleList, orderBy[index], orderBySecond[index2], asc2);

    var contentDiv = $('#Content');
    contentDiv.html('');

    contentDiv.html(contentDiv.html() + `
    <h1>
        Mes Articles
    </h1>
    `);
    DisplayArticlesBySortedValue(ArticleListSorted, asc);
}

$(document).ready(function() {
    Display()
});
