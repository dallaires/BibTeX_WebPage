function InitAllArticles(articlesContainerToUpdate) {
    for (article of articlesContainerToUpdate.Articles) {
        if (article.BibTexRef.length > 0) {
            // Declaring programed type
            article.LocalReferedBy = {};

            article.BibTex = BibTeX[article.BibTexRef]
            if(article.BibTex == undefined) {
                throw "Could not find bibtex for : " + article.BibTexRef;
            }

            article.BibTex["FirstAuthor"] = article.BibTex.Authors[0];
            article.BibTex["BibTexRef"] = article.BibTexRef

            var fileName = article.LocalPdfName;
            if(fileName == undefined || fileName == "") {
                fileName = article.BibTexRef;
            }

            var publisher = article.BibTex.Journal;
            if(publisher == undefined) {
                publisher = article.BibTex.Booktitle;
            }
            if(publisher == undefined) {
                publisher =  article.BibTex.Publisher;
            }
            if(publisher == undefined) {
                publisher =  article.BibTex.School;
            }
            if(publisher == undefined) {
                publisher = article.BibTex.Organization;
            }

            for(var i = 0; i < article.Images.length; i++) {
                article.Images[i] = articlesContainerToUpdate.ResourcesPath + article.Images[i]
            }
            for(var i = 0; i < article.VideosMp4.length; i++) {
                article.VideosMp4[i] = articlesContainerToUpdate.ResourcesPath + article.VideosMp4[i] + ".mp4"
            }

            var subFolder = "";
            var subfolderList = article.Category.split("_");
            subfolderList.pop();
            if(subfolderList.length > 0) {
                subFolder = subfolderList.join("/") + "/";
            }
            article.MappedValues = {
                "FileUrl": article.Url == undefined || article.Url == ""? articlesContainerToUpdate.MainPath + subFolder + fileName + ".pdf" : article.Url,
                "Title": article.BibTex.Title,
                "HtmlNotes": article.HtmlNotes,
                "Category": article.Category,
                "Publisher": publisher,
                "BibTexRef": article.BibTexRef,
                "UrlDate": article.BibTex.UrlDate
            };

            // To fix the 404 on the date
            if (article.BibTex["Url"] == undefined || article.BibTex["Url"] == "") {
                article.BibTex["Url"] = article.MappedValues.FileUrl;
            }

            article.AvailableSortValues = {
                "Category": article.Category,
                "Publisher": article.MappedValues.Publisher,
                "UrlDate": article.MappedValues.UrlDate,
		"Year": article.BibTex.Year
            };

            for(var key in article.AvailableSortValues) {
                if(  article.AvailableSortValues[key] == undefined
                  || article.AvailableSortValues[key] == ""){
                    throw 'The article "' + article.BibTexRef + '" is missing a value for "' + key + '"'; 
                }
            }
        }
    }
}

// TODO Faible, car change le type de LocalReferences et on ne valide pas que l'article sera trouvé...
function InitAllArticlesReferences(articlesContainerToUpdate) {
    var articleIndexByBibTexRef = {};
    for (var i = 0; i < articlesContainerToUpdate.Articles.length; i++) {
        var article = articlesContainerToUpdate.Articles[i];
        if (article.BibTexRef.length > 0) {
            articleIndexByBibTexRef[article.BibTexRef] = i;
        }
    }
    for (article of articlesContainerToUpdate.Articles) {
        var referenceDict = {};
        if(article.LocalReferences == undefined) {
            throw 'LocalReferences for "' + article.BibTexRef + '" is undefined'; 
        }
        for (localReference of article.LocalReferences) {
            var articleIndex = articleIndexByBibTexRef[localReference];
            var referedArticle = articlesContainerToUpdate.Articles[articleIndex];
            referenceDict[localReference] = referedArticle;
            referedArticle.LocalReferedBy[article.BibTexRef] = article;
        }
        article.LocalReferences = referenceDict;
    }
}

function InitArticleContainer(articlesContainerToUpdate) {
    InitAllArticles(articlesContainerToUpdate);
    InitAllArticlesReferences(articlesContainerToUpdate)
}

function AddArticleToDictionary(key, dictionaryToUpdate) {
    var tempTable = dictionaryToUpdate[key];
    if (tempTable == undefined) {
        tempTable = [];
    }
    tempTable[tempTable.length] = article;
    dictionaryToUpdate[key] = tempTable;
}

function SortArticleDictionary(dictionaryToUpdate, secondSortValue, asc) {
    for (var key in dictionaryToUpdate) {
        arrayToSort = dictionaryToUpdate[key];
        dictionaryToUpdate[key] = arrayToSort.sort(function(a, b) {
            if(asc){
                return a.BibTex[secondSortValue].localeCompare(b.BibTex[secondSortValue]);
            }
            else {
                return -a.BibTex[secondSortValue].localeCompare(b.BibTex[secondSortValue]);
            }
        });
    }
}

function InitArticlesByMappedKey(articlesContainer, mappedKey, secondSortValue, asc) {
    var containerToUpdate = {};
    for (article of articlesContainer.Articles) {
        if (article.BibTexRef.length > 0) {
            AddArticleToDictionary(article.AvailableSortValues[mappedKey], containerToUpdate);
        }
    }
    SortArticleDictionary(containerToUpdate, secondSortValue, asc);
    return containerToUpdate;
}

function MapToText(textToUpdate, dictionaryMapper) {
    for (var mappedKey in dictionaryMapper) {
        if (dictionaryMapper[mappedKey] != undefined) {
            textToUpdate = textToUpdate.replace("{" + mappedKey + "}", dictionaryMapper[mappedKey]);
        }
    }
    return textToUpdate;
}
function GetResourcesTemplate(article) {
    var videoTemplate = $("#VideoTemplate").html();
    var imageTemplate = $('#ImageTemplate').html();
    var returnValue = "";

    for(var i = 0; i < article.VideosMp4.length; i++) {
        returnValue += videoTemplate.replace("{VideosMp4}", article.VideosMp4[i]);
    }
    for(var i = 0; i < article.Images.length; i++) {
        returnValue += imageTemplate.replace("{ImagesPng}", article.Images[i]);
    }

    return returnValue;
}


function DisplayArticlesBySortedValue(sortedArticlesDictionary, asc) {
    var contentDiv = $('#Content');
    var headerTemplate = $('#HeaderTemplate').html();
    var articleTemplate = $('#ArticleTemplate').html();
    var referenceTemplate = $("#ReferenceTemplate").html();

    var contentDivBuilder = contentDiv.html();
    var sortedKeys = [];
    for(var key in sortedArticlesDictionary) {
        sortedKeys[sortedKeys.length] = key;
    }
    sortedKeys = sortedKeys.sort(function(a, b) {
        if(asc){
            return a.localeCompare(b);
        }
        else {
            return -a.localeCompare(b);
        }
    });
    
    for (var ki = 0; ki < sortedKeys.length; ki++) {
        var key = sortedKeys[ki];
        headerHtml = headerTemplate.replace("{Title}", key);
        contentDivBuilder = contentDivBuilder + headerHtml;
        
        for (var article of sortedArticlesDictionary[key]) {
            var referenceHtml = MapToText(referenceTemplate, article.BibTex);
            article.MappedValues["ReferenceTemplate"] = referenceHtml;
            article.MappedValues["ResourcesTemplate"] = GetResourcesTemplate(article);

            var articleHtml = MapToText(articleTemplate, article.MappedValues);
            contentDivBuilder = contentDivBuilder + articleHtml;
        }
    }
    contentDiv.html(contentDivBuilder);
}
