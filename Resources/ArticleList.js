ArticleList = {
    MainPath: "References/",
    ResourcesPath: "References/_Resources/",
    Articles: [{
        BibTexRef: "Maerlender2019", // Bibtex label. The local PDF must be inside the directory {MainPath} and named {BibTexRef}.pdf
        Relevance: 0,                // Weight of this paper
        Category: "Biomedical",      // Custom Category. Note: If the category has underscore like "SubDirectoryName_CategoryName", The PDF must be located in the subdirectory {MainPath}/{SubDirectoryName}/{BibTexRef}.pdf
        LocalReferences: [ ],        // This paper cite an other {BibTexRef}
        Images: [ "ConcussionCover.jpg" ], // Image located in {ResourcesPath}. It is easier to find a paper with an image or a video than with text.
        VideosMp4: [ "Example" ],    // Video located in "ResourcesPath". Usefull if the paper is distributed with video.
        HtmlNotes: `
        Custom comments for this paper. It is plaine HTML so you can add whatever you want like links to resources and embeded Youtube video.
        <br />
        You can go on w3schools website for an <a href="https://www.w3schools.com/html/default.asp">HTML tutorial</a>.
        <br />
        The file "Index.html" contains only templates. The page is builded on load.
        <br />
        The page will not load if the Bibtex content is not well formed or if it is missing some attribues (ex: "urldate" wich indicate when the paper was found).
        <br />
        <br />
        Videos and images:
        `
    }, {
        BibTexRef: "",
        Relevance: 0,
        Category: "",
        LocalReferences: [],
        Images: [],
        VideosMp4: [],
        HtmlNotes: ``
    }, {
        BibTexRef: "",
        Relevance: 0,
        Category: "",
        LocalReferences: [],
        Images: [],
        VideosMp4: [],
        HtmlNotes: ``
    }
]
}
