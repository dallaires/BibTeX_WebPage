# Bib

Simple bibliography website with bibtex support. The website contains an open access paper as an example.

To see the website, just open the Index.html page with your prefered web browser. When you add a paper, add the BibTeX reference inside the "BibTeX.js" file then update the file "ArticleList.js" with your coments. Keep a PDF backup inside the directory "References" when possible to avoid losing the access.

The function "CreateBibTex" inside "BibTeX.js" parse raw "BibTeX" content separated with brakets: {}; do not use quotes. Every fields must fit on a single line like the example. It may be a good idea to be able to export the content instead of doing it manually...