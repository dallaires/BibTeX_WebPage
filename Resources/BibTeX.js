// Les valeurs de l'article doivent être impérativement séparies par des retours charriot
function ToDictionary(bibtex) {
    var result = {}
    var reg = /([-a-z]+)\s*=(?:{|"|\s)*(.*?)(?:}|"|\s|,)*\r?\n/gim;
    while (true) {
        match = reg.exec(bibtex);
        if (match != null) {
            var key = match[1].toLowerCase().trim();
            var value = match[2].replace(/({|}|")/gmi, "").trim();

            var isIntegerRegex = /^[0-9]+$/g;
            if (isIntegerRegex.test(value)) {
                 value = Number(value)
            }
            if(key == "keywords" && result[key] != undefined) {
                value = result[key] + "," + value;
            }
            result[key] = value;

        } else {
            return result;
        }
    }
}

function CreateBibTex(bibtexString) {
    var result = {}
    var allReferences = bibtexString.split(/@(ARTICLE|BOOK|BOOKLET|CONFERENCE|INBOOK|INCOLLECTION|INPROCEEDINGS|MANUAL|MASTERSTHESIS|MISC|PHDTHESIS|PROCEEDINGS|TECHREPORT|UNPUBLISHED)/gim);
    //reference
    for(var i=1; i < allReferences.length; i+=2) {
        var referenceType = allReferences[i];
        var bibtex = allReferences[i+1];
        var referenceName = /{\s*?(.+)\s*?,/gim.exec(bibtex)[1]
        if(result[referenceName] == undefined) {
            var tempRef = ToDictionary(bibtex);
            result[referenceName] = {
                // default value: undefined
                "RefType": referenceType,
                "BibTeXRef": referenceName,
                "Authors": tempRef["author"].split(/\s*and\s*/),
                "Title": tempRef["title"],
                "Journal": tempRef["journal"],
                "Booktitle": tempRef["booktitle"],
                "Volume": tempRef["volume"],
                "Number": tempRef["number"],
                "Pages": tempRef["pages"],
                "Abstract": tempRef["abstract"],
                "Year": tempRef["year"],
                "Isbn": tempRef["isbn"],
                "Doi": tempRef["doi"],
                "Url": tempRef["url"],
                "Eprint": tempRef["eprint"],
                "UrlDate": tempRef["urldate"],
                "School": tempRef["school"],
                "Publisher": tempRef["publisher"],
                "Organization": tempRef["organization"]
            };
            if(result[referenceName] == undefined) {
                throw "The reference {" + referenceName + "} was not created";    
            }
        }
        else {
            throw "The reference {" + referenceName + "} already exists: "; 
        }
    }
    return result;
}

BibTeX = CreateBibTex(`
@Article{Maerlender2019,
    author = {Maerlender, Arthur and Lichtenstein, Jonathan D and Parent-Nichols, Jennifer  and Higgins, Kate and Reisher, Peggy},
    title = {Concussion competencies: a training model for school-based concussion management},
    journal = {Concussion},
    volume = {0},
    number = {0},
    pages = {null},
    year = {2019},
    doi = {10.2217/cnc-2018-0008},
    URL = {https://doi.org/10.2217/cnc-2018-0008},
    eprint = {https://doi.org/10.2217/cnc-2018-0008},
    abstract = {This study reports on the use of ten knowledge competencies related to the behavioral management of concussion in schools. Trainings using these competencies as learning objectives were delivered to school personnel. This aims of the use of competencies in this way are to streamline the education of key stakeholders, to establish clear roles and responsibilities for constituents and equip individuals working with students following a concussion with the relevant knowledge to optimize outcomes. The majority of participants, primarily speech language pathologists working as related service providers in the schools where the trainings occurred, judged the use of the competencies to be informative and useful to their practice both immediately following the training and at a 5-month follow-up. The greatest gains in knowledge were noted by those participants self-reporting the least amount of knowledge pre-training. Participants also ranked the perceived value and relative importance of each of the ten competencies.},
    urldate = {2019-06-12},
}
`);
